﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Rest_with_ASP.Net_Core_and_Docker {
    public class Program {
        public static void Main(string[] args) {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
