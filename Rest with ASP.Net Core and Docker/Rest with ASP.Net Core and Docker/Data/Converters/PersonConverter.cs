﻿using System.Collections.Generic;
using System.Linq;
using Rest_with_ASP.Net_Core_and_Docker.Data.Converter;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using Rest_with_ASP.Net_Core_and_Docker.Model;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.Converters {
    public class PersonConverter : IParser<PersonVO, Person>, IParser<Person, PersonVO> {
        public Person Parse(PersonVO origin) {
            if (origin == null) return new Person();
            return new Person {
                Id = origin.Id,
                FirstName = origin.FirstName,
                LastName = origin.LastName,
                Address = origin.Address,
                Gender = origin.Gender
            };
        }

        public PersonVO Parse(Person origin) {
            if (origin == null) return new PersonVO();
            return new PersonVO {
                Id = origin.Id,
                FirstName = origin.FirstName,
                LastName = origin.LastName,
                Address = origin.Address,
                Gender = origin.Gender
            };
        }

        public List<Person> ParseList(List<PersonVO> origin) {
            if (origin == null) return new List<Person>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }

        public List<PersonVO> ParseList(List<Person> origin) {
            if (origin == null) return new List<PersonVO>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }
    }
}
