﻿using System.Collections.Generic;
using System.Linq;
using Rest_with_ASP.Net_Core_and_Docker.Data.Converter;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using Rest_with_ASP.Net_Core_and_Docker.Model;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.Converters {
    public class BookConverter : IParser<BookVO, Book>, IParser<Book, BookVO> {
        public BookVO Parse(Book origin) {
            if (origin == null) return new BookVO();
            return new BookVO {
                Id = origin.Id,
                Title = origin.Title,
                Author = origin.Author,
                LaunchDate = origin.LaunchDate,
                Price = origin.Price
            };
        }

        public List<BookVO> ParseList(List<Book> origin) {
            if (origin == null) return new List<BookVO>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }

        public Book Parse(BookVO origin) {
            if (origin == null) return new Book();
            return new Book {
                Id = origin.Id,
                Title = origin.Title,
                Author = origin.Author,
                LaunchDate = origin.LaunchDate,
                Price = origin.Price
            };
        }

        public List<Book> ParseList(List<BookVO> origin) {
            if (origin == null) return new List<Book>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }
    }
}
