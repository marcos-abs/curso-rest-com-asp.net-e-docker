﻿using Rest_with_ASP.Net_Core_and_Docker.Data.Converter;
using Rest_with_ASP.Net_Core_and_Docker.Model;
using System.Collections.Generic;
using System.Linq;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.Converters {
    public class UserConverter : IParser<UserVO, User>, IParser<User, UserVO> {
        public User Parse(UserVO origin) {
            if (origin == null) return new User();
            return new User {
                Login = origin.Login,
                AccessKey = origin.AccessKey
            };
        }

        public UserVO Parse(User origin) {
            if (origin == null) return new UserVO();
            return new UserVO {
                Login = origin.Login,
                AccessKey = origin.AccessKey
            };
        }

        public List<User> ParseList(List<UserVO> origin) {
            if (origin == null) return new List<User>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }

        public List<UserVO> ParseList(List<User> origin) {
            if (origin == null) return new List<UserVO>();
            return origin.Select(item => Parse(item)).ToList(); // Internamente é como se o Linq executasse um foreach convertendo acada item da lista.
        }
    }
}
