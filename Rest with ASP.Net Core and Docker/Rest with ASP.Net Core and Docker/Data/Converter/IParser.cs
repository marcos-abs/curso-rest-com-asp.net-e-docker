﻿using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.Converter {
    public interface IParser<O, D> {
        D Parse(O origin);
        List<D> ParseList(List<O> origin);
    }
}
