﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rest_with_ASP.Net_Core_and_Docker.Model {
    public class UserVO {
        public string Login { get; set; }
        public string AccessKey { get; set; }
    }
}
