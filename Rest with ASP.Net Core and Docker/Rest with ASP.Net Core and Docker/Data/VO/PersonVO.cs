﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tapioca.HATEOAS;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.VO {

    [DataContract]
    public class PersonVO : ISupportsHyperMedia {

        [DataMember(Order = 1, Name = "codigo")]
        public long? Id { get; set; }

        [DataMember(Order = 2, Name = "nome", IsRequired = true) ]
        public string FirstName { get; set; }

        [DataMember(Order = 3, Name = "sobrenome", IsRequired = true)]
        public string LastName { get; set; }

        [DataMember(Order = 4, Name = "endereco", IsRequired = true)]
        public string Address { get; set; }

        [DataMember(Order = 5, Name = "sexo", IsRequired = false)]
        public string Gender { get; set; }

        [DataMember(Order = 6, Name = "links")]
        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}
