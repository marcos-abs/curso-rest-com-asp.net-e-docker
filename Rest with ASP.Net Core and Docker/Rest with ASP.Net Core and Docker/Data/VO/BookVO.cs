﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tapioca.HATEOAS;

namespace Rest_with_ASP.Net_Core_and_Docker.Data.VO {

    [DataContract]
    public class BookVO : ISupportsHyperMedia {

        [DataMember (Order = 1, Name = "codigo")]
        public long? Id { get; set; }

        [DataMember(Order = 2, Name = "titulo", IsRequired = true)]
        public string Title { get; set; }

        [DataMember(Order = 3, Name = "autor", IsRequired = true)]
        public string Author { get; set; }

        [DataMember(Order = 5, Name = "preco", IsRequired = false)]
        public decimal Price { get; set; }

        [DataMember(Order = 4, Name = "publicacao", IsRequired = false)]
        public DateTime LaunchDate { get; set; }

        [DataMember(Order = 6, Name = "links")]
        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}
