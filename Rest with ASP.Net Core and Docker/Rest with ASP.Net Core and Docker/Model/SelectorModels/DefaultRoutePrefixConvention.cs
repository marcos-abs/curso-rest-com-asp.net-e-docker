﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

/*
 * Fonte: https://tpodolak.com/blog/2017/05/03/asp-net-core-default-api-version-with-url-path-versioning/
 */

namespace Rest_with_ASP.Net_Core_and_Docker.Model.SelectorModels {
    public class DefaultRoutePrefixConvention : IApplicationModelConvention {
        public void Apply(ApplicationModel application) {
            foreach (var applicationController in application.Controllers) {
                applicationController.Selectors.Add(new SelectorModel {
                    AttributeRouteModel = new AttributeRouteModel {
                        Template = "[controller]"
                    }
                });
            }
        }
    }
}
