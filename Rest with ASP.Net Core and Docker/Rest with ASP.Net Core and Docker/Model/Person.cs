﻿using Rest_with_ASP.Net_Core_and_Docker.Model.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rest_with_ASP.Net_Core_and_Docker.Model {

    [Table("persons")]
    public class Person : BaseEntity {

        [Column("FirstName")]
        public string FirstName { get; set; }

        [Column("LastName")]
        public string LastName { get; set; }

        [Column("Address")]
        public string Address { get; set; }

        [Column("Gender")]
        public string Gender { get; set; }
    }
}
