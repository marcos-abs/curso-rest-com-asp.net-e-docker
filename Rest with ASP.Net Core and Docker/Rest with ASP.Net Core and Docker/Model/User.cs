﻿namespace Rest_with_ASP.Net_Core_and_Docker.Model {
    public class User {
        public long? Id { get; set; }
        public string Login { get; set; }
        public string AccessKey { get; set; }
    }
}
