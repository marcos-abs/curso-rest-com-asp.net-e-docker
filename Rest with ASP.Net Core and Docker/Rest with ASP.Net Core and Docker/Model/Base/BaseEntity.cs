﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Rest_with_ASP.Net_Core_and_Docker.Model.Base {
    //[DataContract] /* Contrato entre os atributos do objeto e a estrutura de dados no banco de dados */
    public class BaseEntity {

        [Column("id")]
        public long? Id { get; set; }
    }
}
