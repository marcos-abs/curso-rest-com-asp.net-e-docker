﻿using Rest_with_ASP.Net_Core_and_Docker.Model;
using Rest_with_ASP.Net_Core_and_Docker.Model.Context;
using Rest_with_ASP.Net_Core_and_Docker.Repository.Generic;
using System.Linq;
using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Business.Implementations {
#pragma warning disable IDE1006 // Estilos de Nomenclatura
    public class PersonRepositoryImpl : GenericRepository<Person>, IPersonRepository {
#pragma warning restore IDE1006 // Estilos de Nomenclatura

        public PersonRepositoryImpl(MySQLContext context) : base (context) { }

        List<Person> IPersonRepository.FindByName(string firstName, string lastName) {
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName)) {
                return _context.Persons.Where(p => p.FirstName.Contains(firstName) && p.LastName.Contains(lastName)).ToList();
            } else if (string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName)) {
                return _context.Persons.Where(p => p.LastName.Contains(lastName)).ToList();
            } else if (!string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName)) {
                return _context.Persons.Where(p => p.FirstName.Contains(firstName)).ToList();
            }
            return _context.Persons.ToList();
        }
    }
}