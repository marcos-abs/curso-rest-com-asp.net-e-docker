﻿using Rest_with_ASP.Net_Core_and_Docker.Model;
using Rest_with_ASP.Net_Core_and_Docker.Model.Context;
using System.Linq;

namespace Rest_with_ASP.Net_Core_and_Docker.Business.Implementations {
#pragma warning disable IDE1006 // Estilos de Nomenclatura
    public class UserRepositoryImpl : IUserRepository {
#pragma warning restore IDE1006 // Estilos de Nomenclatura
        private readonly MySQLContext _context;

        public UserRepositoryImpl(MySQLContext context) {
            _context = context;
        }

        public User FindByLogin(string login) {
            return _context.Users.SingleOrDefault(u => u.Login.Equals(login));
        }
    }
}