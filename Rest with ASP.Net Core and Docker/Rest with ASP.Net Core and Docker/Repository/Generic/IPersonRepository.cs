﻿using Rest_with_ASP.Net_Core_and_Docker.Model;
using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Repository.Generic {
    public interface IPersonRepository : IRepository<Person> {
#pragma warning disable IDE1006 // Estilos de Nomenclatura
        List<Person> FindByName(string firstName, string lastName);
#pragma warning restore IDE1006 // Estilos de Nomenclatura
    }
}
