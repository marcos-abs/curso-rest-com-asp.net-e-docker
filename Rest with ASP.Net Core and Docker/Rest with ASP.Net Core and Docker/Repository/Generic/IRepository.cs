﻿using Rest_with_ASP.Net_Core_and_Docker.Model.Base;
using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Repository.Generic {
    public interface IRepository<T> where T : BaseEntity {
        T Create(T item);
        T FindById(long id);
        List<T> FindAll();
        T Update(T item);
        void Delete(long id);

        bool Exists(long? id);
        List<T> FindWithPageSearch(string query);
        int GetCount(string query);
    }
}
