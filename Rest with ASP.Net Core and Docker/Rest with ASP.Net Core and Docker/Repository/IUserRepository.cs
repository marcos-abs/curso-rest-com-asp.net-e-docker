﻿namespace Rest_with_ASP.Net_Core_and_Docker.Model {
    public interface IUserRepository {
        User FindByLogin(string login);
    }
}