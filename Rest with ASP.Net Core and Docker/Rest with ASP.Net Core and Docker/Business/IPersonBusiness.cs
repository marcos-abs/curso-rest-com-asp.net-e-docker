﻿using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using System.Collections.Generic;
using Tapioca.HATEOAS.Utils;

namespace Rest_with_ASP.Net_Core_and_Docker.Business {
    public interface IPersonBusiness {
        PersonVO Create(PersonVO person);
        PersonVO FindById(long id);
        List<PersonVO> FindAll();
        List<PersonVO> FindByName(string firstName, string lastName);
        PersonVO Update(PersonVO person);
        void Delete(long id);
        PagedSearchDTO<PersonVO> FindWithPagedSearch(string name, string sortDirection, int pageSize, int page);
    }
}
