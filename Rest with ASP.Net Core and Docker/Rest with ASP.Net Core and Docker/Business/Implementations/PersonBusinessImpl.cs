﻿using Rest_with_ASP.Net_Core_and_Docker.Data.Converters;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using Rest_with_ASP.Net_Core_and_Docker.Model;
using Rest_with_ASP.Net_Core_and_Docker.Repository.Generic;
using System.Collections.Generic;
using Tapioca.HATEOAS.Utils;

namespace Rest_with_ASP.Net_Core_and_Docker.Business.Implementations {
    public class PersonBusinessImpl : IPersonBusiness {
        private IPersonRepository _repository;

        private readonly PersonConverter _converter;

        public PersonBusinessImpl(IPersonRepository repository) { 
            _repository = repository;
            _converter = new PersonConverter();
        }

        public PersonVO Create(PersonVO person) {
            var personEntity = _converter.Parse(person);
            personEntity = _repository.Create(personEntity); // na verdade ele converte Entity para VO para responder corretamente ao controller.
            return _converter.Parse(personEntity);
        }

        public PersonVO FindById(long id) {
            return _converter.Parse(_repository.FindById(id));
        }

        public List<PersonVO> FindAll() {
            return _converter.ParseList(_repository.FindAll());
        }

        public List<PersonVO> FindByName(string firstName, string lastName) {
            return _converter.ParseList(_repository.FindByName(firstName, lastName));
        }

        public PersonVO Update(PersonVO person) {
            var personEntity = _converter.Parse(person);
            personEntity = _repository.Update(personEntity); // na verdade ele converte Entity para VO para responder corretamente ao controller.
            return _converter.Parse(personEntity);
        }

        public void Delete(long id) {
            _repository.Delete(id);
        }

#pragma warning disable IDE1006 // Estilos de Nomenclatura
        public PagedSearchDTO<PersonVO> FindWithPagedSearch(string name, string sortDirection, int pageSize, int page) {
#pragma warning restore IDE1006 // Estilos de Nomenclatura
            page = page > 0 ? page - 1 : 0; // compatibilizar com a forma de paginação do banco de dados (parte 1 de 2)
            string query = @"select * from Persons p where 1 = 1 ";
            if (!string.IsNullOrEmpty(name)) query = query + $" and p.firstName like '%{name}%'";
            query = query + $" order by p.FirstName {sortDirection} limit {pageSize} offset {page}";

            string countQuery = @"select count(*) from Persons p where 1 = 1 ";
            if (!string.IsNullOrEmpty(name)) countQuery = countQuery + $" and p.firstName like '%{name}%'";

            var persons = _repository.FindWithPageSearch(query);

            int totalResults = _repository.GetCount(countQuery);

            return new PagedSearchDTO<PersonVO> {
                CurrentPage = page + 1,  // compatibilizar com a forma de paginação do banco de dados (parte 2 de 2) ;-)
                List = _converter.ParseList(persons),
                PageSize = pageSize,
                SortDirections = sortDirection,
                TotalResults = totalResults
            };
        }
        private bool Exist(long? id) {
            return _repository.Exists(id);
        }
    }
}