﻿using Rest_with_ASP.Net_Core_and_Docker.Data.Converters;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using Rest_with_ASP.Net_Core_and_Docker.Model;
using Rest_with_ASP.Net_Core_and_Docker.Repository.Generic;
using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Business.Implementations {
    public class BookBusinessImpl : IBookBusiness {
        private IRepository<Book> _repository;
        
        private readonly BookConverter _converter;
        
        public BookBusinessImpl(IRepository<Book> repository) {
            _repository = repository;
            _converter = new BookConverter();
        }

        public BookVO Create(BookVO book) {
            var bookEntity = _converter.Parse(book);
            bookEntity = _repository.Create(bookEntity); // na verdade ele converte Entity para VO para responder corretamente ao controller.
            return _converter.Parse(bookEntity);
        }

        public void Delete(long id) {
            _repository.Delete(id);
        }

        public List<BookVO> FindAll() {
            return _converter.ParseList(_repository.FindAll());
        }

        public BookVO FindById(long id) {
            return _converter.Parse(_repository.FindById(id));
        }

        public BookVO Update(BookVO book) {
            var bookEntity = _converter.Parse(book);
            bookEntity = _repository.Update(bookEntity); // na verdade ele converte Entity para VO para responder corretamente ao controller.
            return _converter.Parse(bookEntity);
        }

        private bool Exist(long? id) {
            return _repository.Exists(id);
        }
    }
}