﻿using Rest_with_ASP.Net_Core_and_Docker.Model;
using System.IO;

namespace Rest_with_ASP.Net_Core_and_Docker.Business.Implementations {
#pragma warning disable IDE1006 // Estilos de Nomenclatura
    public class FileBusinessImpl : IFileBusiness {
#pragma warning restore IDE1006 // Estilos de Nomenclatura
        public byte[] GetPDFFile() {
            string path = Directory.GetCurrentDirectory();
            var fullPath = path + "\\Other\\aspnet-life-cycles-events.pdf";
            return File.ReadAllBytes(fullPath);
        }
    }
}