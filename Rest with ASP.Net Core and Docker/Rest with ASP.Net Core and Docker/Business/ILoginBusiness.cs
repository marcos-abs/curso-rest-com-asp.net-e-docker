﻿using Rest_with_ASP.Net_Core_and_Docker.Model;

namespace Rest_with_ASP.Net_Core_and_Docker.Business {
    public interface ILoginBusiness {
        object FindByLogin(UserVO user);
    }
}
