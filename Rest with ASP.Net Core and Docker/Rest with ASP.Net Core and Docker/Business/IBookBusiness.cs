﻿using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using System.Collections.Generic;

namespace Rest_with_ASP.Net_Core_and_Docker.Business {
    public interface IBookBusiness {
        BookVO Create(BookVO book);
        BookVO FindById(long id);
        List<BookVO> FindAll();
        BookVO Update(BookVO book);
        void Delete(long id);
    }
}
