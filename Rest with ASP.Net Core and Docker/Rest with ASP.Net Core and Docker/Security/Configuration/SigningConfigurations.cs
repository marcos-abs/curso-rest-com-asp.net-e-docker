﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace Rest_with_ASP.Net_Core_and_Docker.Security.Configuration {
    public class SigningConfigurations {
        public SecurityKey key { get; }
        public SigningCredentials SigningCredentials { get; }

        public SigningConfigurations() {
            using (var provider = new RSACryptoServiceProvider(2048)) {
                key = new RsaSecurityKey(provider.ExportParameters(true));
            }
            SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}
