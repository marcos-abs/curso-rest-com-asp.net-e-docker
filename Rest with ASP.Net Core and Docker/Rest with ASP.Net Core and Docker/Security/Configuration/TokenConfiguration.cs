﻿namespace Rest_with_ASP.Net_Core_and_Docker.Security.Configuration {
    public class TokenConfiguration {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}
