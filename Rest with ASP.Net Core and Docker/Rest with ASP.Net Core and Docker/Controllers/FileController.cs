﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rest_with_ASP.Net_Core_and_Docker.Business;
using Tapioca.HATEOAS;

namespace Rest_with_ASP.Net_Core_and_Docker.Controllers {
    [ApiVersion("1")] // versão da api
    [Route("api/[controller]/v{version:apiVersion}")] // url da api com versionamento
    public class FileController : Controller {
        private IFileBusiness _fileBusiness;

        public FileController(IFileBusiness fileBusiness) {
            _fileBusiness = fileBusiness;
        }

        [HttpGet]
        [ProducesResponseType(typeof(byte[]), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
#pragma warning disable IDE1006 // Estilos de Nomenclatura
        public IActionResult GetPDFFile() {
#pragma warning restore IDE1006 // Estilos de Nomenclatura
            byte[] buffer = _fileBusiness.GetPDFFile();
            if(buffer != null) {
                HttpContext.Response.ContentType = "application/pdf";
                HttpContext.Response.Headers.Add("content-lenght", buffer.Length.ToString());
                HttpContext.Response.Body.Write(buffer, 0, buffer.Length);
            }
            return new ContentResult();
        }
    }
}
