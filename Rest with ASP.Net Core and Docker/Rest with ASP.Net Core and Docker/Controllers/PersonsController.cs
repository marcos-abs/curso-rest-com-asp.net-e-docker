﻿using Microsoft.AspNetCore.Mvc;
using Rest_with_ASP.Net_Core_and_Docker.Business;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using System.Collections.Generic;
using Tapioca.HATEOAS;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Authorization;

namespace Rest_with_ASP.Net_Core_and_Docker.Controllers {
    /* 
     * Mapeia as requisições de http://localhost:{porta}/api/persons/v1/
     * Por padrão o ASP.NET Core mapeia todas as classes que extendem Controller
     * pegando a primeira parte do nome da classe em lower case [Person]Controller
     * e expõe como endpoint REST
     */
    [ApiVersion("1")] // versão da api
    [Route("api/[controller]/v{version:apiVersion}")] // url da api com versionamento
    public class PersonsController : Controller {
        //Declaração do serviço usado
        private IPersonBusiness _personBusiness;

        /* Injeção de uma instancia de IPersonService ao criar
        uma instancia de PersonController */
        public PersonsController(IPersonBusiness personBusiness) {
            _personBusiness = personBusiness;
        }

        //Mapeia as requisições GET para http://localhost:{porta}/api/person/
        //Get sem parâmetros para o FindAll --> Busca Todos
        [HttpGet]
        //[SwaggerResponse(200, TypeFilterAttribute = typeof(List<PersonVO>))] // trocou para ProducesResponseType na versão 4.0.1 (fonte: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/issues/159)
        [ProducesResponseType(typeof(List<PersonVO>), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get() {
            return new OkObjectResult(_personBusiness.FindAll());
        }

        //Mapeia as requisições GET para http://localhost:{porta}/api/person/{id}
        //recebendo um ID como no Path da requisição
        //Get com parâmetros para o FindById --> Busca Por ID
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PersonVO), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id) {
            var person = _personBusiness.FindById(id);
            if (person == null) return NotFound();
            return new OkObjectResult(person);
        }

        //Mapeia as requisições GET para http://localhost:{porta}/api/person/{id}
        //recebendo um ID como no Path da requisição
        //Get com parâmetros para o FindById --> Busca Por ID
        [HttpGet("find-by-name")]
        [ProducesResponseType(typeof(PersonVO), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
#pragma warning disable IDE1006 // Estilos de Nomenclatura
        public IActionResult GetByName([FromQuery] string firstName, [FromQuery] string lastName) {
#pragma warning restore IDE1006 // Estilos de Nomenclatura
            return new OkObjectResult(_personBusiness.FindByName(firstName, lastName));
        }

        [HttpGet("find-with-paged-search/{sortDiretion}/{pageSize}/{page}")]
        [ProducesResponseType(typeof(PersonVO), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
#pragma warning disable IDE1006 // Estilos de Nomenclatura
        public IActionResult GetPagedSearch([FromQuery] string name, string sortDirection, int pageSize, int page) {
 #pragma warning restore IDE1006 // Estilos de Nomenclatura
            return new OkObjectResult(_personBusiness.FindWithPagedSearch(name, sortDirection, pageSize, page));
        }

        //Mapeia as requisições POST para http://localhost:{porta}/api/person/
        //O [FromBody] consome o Objeto JSON enviado no corpo da requisição
        [HttpPost]
        [ProducesResponseType(typeof(PersonVO), 201)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]PersonVO person) {
            if (person == null) return BadRequest();
            return new OkObjectResult(_personBusiness.Create(person));
        }

        //Mapeia as requisições PUT para http://localhost:{porta}/api/person/
        //O [FromBody] consome o Objeto JSON enviado no corpo da requisição
        [HttpPut]
        [ProducesResponseType(typeof(PersonVO), 202)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]PersonVO person) {
            if (person == null) return BadRequest();
            var updatedPerson = _personBusiness.Update(person);
            if (updatedPerson == null) return NoContent();
            return new OkObjectResult(updatedPerson);
        }

        //Mapeia as requisições PATCH para http://localhost:{porta}/api/person/
        //O [FromBody] consome o Objeto JSON enviado no corpo da requisição
        [HttpPatch]
        [ProducesResponseType(typeof(PersonVO), 202)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Patch([FromBody]PersonVO person) {
            if (person == null) return BadRequest();
            var updatedPerson = _personBusiness.Update(person);
            if (updatedPerson == null) return NoContent();
            return new OkObjectResult(updatedPerson);
        }

        //Mapeia as requisições DELETE para http://localhost:{porta}/api/person/{id}
        //recebendo um ID como no Path da requisição
        [HttpDelete("{id}")]
        [ProducesResponseType(204)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id) {
            _personBusiness.Delete(id);
            return NoContent();
        }
    }
}
