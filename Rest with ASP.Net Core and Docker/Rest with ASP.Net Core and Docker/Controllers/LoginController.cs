﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rest_with_ASP.Net_Core_and_Docker.Business;
using Rest_with_ASP.Net_Core_and_Docker.Model;

namespace Rest_with_ASP.Net_Core_and_Docker.Controllers {
    [ApiVersion("1")] // versão da api
    [Route("api/[controller]/v{version:apiVersion}")] // url da api com versionamento
    public class LoginController : Controller {
        private ILoginBusiness _loginBusiness;

        public LoginController(ILoginBusiness loginBusiness) {
            _loginBusiness = loginBusiness;
        }

        [AllowAnonymous]
        [HttpPost]
        public object Post([FromBody]UserVO user) {
            if (user == null) return BadRequest();
            return _loginBusiness.FindByLogin(user);
        }
    }
}
