﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rest_with_ASP.Net_Core_and_Docker.Business;
using Rest_with_ASP.Net_Core_and_Docker.Data.VO;
using Tapioca.HATEOAS;

namespace Rest_with_ASP.Net_Core_and_Docker.Controllers {
    /* 
    * Mapeia as requisições de http://localhost:{porta}/api/books/v1/
    * Por padrão o ASP.NET Core mapeia todas as classes que extendem Controller
    * pegando a primeira parte do nome da classe em lower case [Book]Controller
    * e expõe como endpoint REST
    */
    [ApiVersion("1")] // versão da api
    [Route("api/[controller]/v{version:apiVersion}")] // url da api com versionamento
    public class BooksController : Controller {
        //Declaração do serviço usado
        private IBookBusiness _bookBusiness;

        /* Injeção de uma instancia de IBookService ao criar
        uma instancia de BookController */
        public BooksController(IBookBusiness bookBusiness) {
            _bookBusiness = bookBusiness;
        }

        //Mapeia as requisições GET para http://localhost:{porta}/api/book/
        //Get sem parâmetros para o FindAll --> Busca Todos
        [HttpGet]
        [ProducesResponseType(typeof(BookVO), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get() {
            return new OkObjectResult(_bookBusiness.FindAll());
        }

        //Mapeia as requisições GET para http://localhost:{porta}/api/book/{id}
        //recebendo um ID como no Path da requisição
        //Get com parâmetros para o FindById --> Busca Por ID
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BookVO), 200)] // versão nova 4.0.1
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id) {
            var book = _bookBusiness.FindById(id);
            if (book == null) return NotFound();
            return new OkObjectResult(book);
        }

        //Mapeia as requisições POST para http://localhost:{porta}/api/book/
        //O [FromBody] consome o Objeto JSON enviado no corpo da requisição
        [HttpPost]
        [ProducesResponseType(typeof(BookVO), 201)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]BookVO book) {
            if (book == null) return BadRequest();
            return new OkObjectResult(_bookBusiness.Create(book));
        }

        //Mapeia as requisições PUT para http://localhost:{porta}/api/book/
        //O [FromBody] consome o Objeto JSON enviado no corpo da requisição
        [HttpPut]
        [ProducesResponseType(typeof(BookVO), 202)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]BookVO book) {
            if (book == null) return BadRequest();
            var updatedBook = _bookBusiness.Update(book);
            if (updatedBook == null) return NoContent();
            return new OkObjectResult(updatedBook);
        }

        //Mapeia as requisições DELETE para http://localhost:{porta}/api/book/{id}
        //recebendo um ID como no Path da requisição
        [HttpDelete("{id}")]
        [ProducesResponseType(204)] // versão nova 4.0.1
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id) {
            _bookBusiness.Delete(id);
            return NoContent();
        }
    }
}
